package ru.anenkov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;

@Component
public class VersionClientListener extends AbstractListenerClient {

    @Override
    public @Nullable String arg() {
        return "-v";
    }

    @Override
    public @Nullable String command() {
        return "Version";
    }

    @Override
    public @Nullable String description() {
        return "Show version app";
    }

    @Async
    @Override
    @EventListener(condition = "@versionClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[VERSION APP]");
        System.out.println("\t1.2.2");
        System.out.println("  [SUCCESS]");
    }

}
