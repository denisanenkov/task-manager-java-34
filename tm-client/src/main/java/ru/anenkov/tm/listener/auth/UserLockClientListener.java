package ru.anenkov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.AdminUserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.Locale;

@Component
public class UserLockClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Lock-user";
    }

    @Override
    public @Nullable String description() {
        return "Lock user (admin command)";
    }

    @Async
    @Override
    @EventListener(condition = "@userLockClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[LOCK USER]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        adminUserEndpoint.lockUserByLogin(bootstrap.getSession(), login);
        System.out.println("[LOCK USER WITH LOGIN \"" + login.toUpperCase(Locale.ROOT) + "\" SUCCESS]");
    }

}
