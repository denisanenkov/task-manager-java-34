package ru.anenkov.tm.listener.data.json;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.AdminEndpoint;
import ru.anenkov.tm.enumeration.Role;

import java.io.IOException;

@Component
public class DataJsonLoadListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String command() {
        return "Data-json-load";
    }

    @Override
    public String description() {
        return "Load data from json file";
    }

    @Async
    @Override
    @EventListener(condition = "@dataJsonLoadListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[DATA JSON LOAD]");
        adminEndpoint.loadDataJson(bootstrap.getSession());
        System.out.println("[OK]");
    }

}
