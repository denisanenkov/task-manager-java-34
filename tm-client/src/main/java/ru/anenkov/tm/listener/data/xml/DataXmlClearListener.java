package ru.anenkov.tm.listener.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.AdminEndpoint;
import ru.anenkov.tm.enumeration.Role;

@Component
public class DataXmlClearListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "Data-xml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from xml file";
    }

    @Async
    @Override
    @EventListener(condition = "@dataXmlClearListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[DATA XML CLEAR]");
        adminEndpoint.clearDataXml(bootstrap.getSession());
        System.out.println("[OK]");
    }

}