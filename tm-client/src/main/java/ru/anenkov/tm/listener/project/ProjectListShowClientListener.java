package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.ProjectDTO;
import ru.anenkov.tm.endpoint.ProjectEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.empty.EmptyListException;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectListShowClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Project-list";
    }

    @Override
    public @Nullable String description() {
        return "Get Project list";
    }

    @Async
    @Override
    @EventListener(condition = "@projectListShowClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[PROJECT LIST]\n");
        int count = 1;
        @Nullable List<ProjectDTO> projectList = projectEndpoint.getListProjects(bootstrap.getSession());
        for (ProjectDTO project : projectList) {
            System.out.println("Number of project: " + (count++) + "\nName project: " + project.getName() +
                    "\nDescription project: " + project.getDescription() + "\n");
        }
        System.out.println("[SUCCESS]");
    }

}
