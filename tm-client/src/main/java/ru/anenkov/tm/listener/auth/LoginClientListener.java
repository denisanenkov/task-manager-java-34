package ru.anenkov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.SessionDTO;
import ru.anenkov.tm.endpoint.SessionEndpoint;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.Locale;
import java.util.Set;

@Component
public class LoginClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public @Nullable String arg() {
        return "-log";
    }

    @Override
    public @Nullable String command() {
        return "Login";
    }

    @Override
    public @Nullable String description() {
        return "Login user";
    }

    @Async
    @Override
    @EventListener(condition = "@loginClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[LOGIN]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final SessionDTO session = sessionEndpoint.openSession(login, password);
        bootstrap.setSession(session);
        if (bootstrap.getSession() != null) System.out.println("[LOGIN SUCCESS]");
    }

}
