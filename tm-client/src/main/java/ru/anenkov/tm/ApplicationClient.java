package ru.anenkov.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.AbstractApplicationContext;
import ru.anenkov.tm.bootstrap.BootstrapClient;
import ru.anenkov.tm.config.AppConfiguration;

public class ApplicationClient {

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
        context.registerShutdownHook();
        context.getBean(BootstrapClient.class).run(args);
    }

}
