import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.marker.*;
import ru.anenkov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Category(AllCategory.class)
public class UserEndpointTest {

    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    private final UserEndpointService userEndpointService = new UserEndpointService();
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @Test
    @Category(SearchCategory.class)
    public void findAllAndCreateUsersTest() {
        final SessionDTO session = sessionEndpoint.openSession("test", "test");
        int old = userEndpoint.findAllUser(session).size();
        userEndpoint.createUser(session, "123", "123");
        Assert.assertEquals(userEndpoint.findAllUser(session).size(), old + 1);
    }

    @Test
    @Category(SearchCategory.class)
    public void findUserByParametersTest() {
        final SessionDTO session = sessionEndpoint.openSession("test", "test");
        UserDTO testUser = userEndpoint.findByLoginUser(session, "1");
        UserDTO user1, user2, user3;
        user1 = userEndpoint.findByIdUser(session, testUser.getId());
        Assert.assertEquals(user1.getLogin(), "1");
        user2 = userEndpoint.findByLoginUser(session, "2");
        Assert.assertNotNull(user2);
        user3 = userEndpoint.findByEmailUser(session, "test@mail.ru");
        Assert.assertEquals(user3.getLogin(), "1");
    }

    @Test
    @Category(SearchCategory.class)
    public void checkRoleUserTest() {
        final SessionDTO session = sessionEndpoint.openSession("test", "test");
        UserDTO user = userEndpoint.findByLoginUser(session, "test");
        user.setRole(Role.USER);
        Assert.assertEquals(user.getRole(), Role.USER);
        user.setRole(Role.ADMIN);
        Assert.assertEquals(user.getRole(), Role.ADMIN);
    }

    @Test
    @Category(SearchCategory.class)
    public void getListUsersTest() {
        final SessionDTO session = sessionEndpoint.openSession("test", "test");
        List<UserDTO> userList = new ArrayList<>();
        Assert.assertTrue(userList.isEmpty());
        userList = userEndpoint.getListUser(session);
        Assert.assertFalse(userList.isEmpty());
    }

    @Test
    @Category(UpdateCategory.class)
    public void updateUserNameParametersTest() {
        final SessionDTO session = sessionEndpoint.openSession("admin", "admin");
        UserDTO user = userEndpoint.findByLoginUser(session, "admin");
        user.setFirstName("");
        user.setMiddleName("");
        user.setLastName("");
        Assert.assertEquals(user.getFirstName(), "");
        Assert.assertEquals(user.getMiddleName(), "");
        Assert.assertEquals(user.getLastName(), "");
        userEndpoint.updateUserFirstName(session, "Denis");
        Assert.assertEquals(user.getFirstName(), "Denis");
        userEndpoint.updateUserMiddleName(session, "Alexandrovich");
        Assert.assertEquals(user.getMiddleName(), "Alexandrovich");
        userEndpoint.updateUserLastName(session, "Anenkov");
        Assert.assertEquals(user.getLastName(), "Anenkov");
    }

    @Test
    @Category(UpdateCategory.class)
    public void updateUserEmail() {
        SessionDTO session = sessionEndpoint.openSession("admin", "admin");
        UserDTO user = userEndpoint.findByLoginUser(session, "admin");
        user.setEmail("");
        userEndpoint.updateUserEmail(session, "newUserEmail@mail.ru");
        Assert.assertNotEquals(user.getEmail(), "newUserEmail@Mail.ru");
        Assert.assertEquals(user.getEmail(), "newUserEmail@mail.ru");
    }

    @Test
    @Category(UpdateCategory.class)
    public void updatePassword() {
        SessionDTO session = sessionEndpoint.openSession("admin", "admin");
        String oldPass = userEndpoint.findByLoginUser(session, "admin").getPasswordHash();
        userEndpoint.updatePasswordUser(session, "newAdmin");
        String newPass = userEndpoint.findByLoginUser(session, "admin").getPasswordHash();
        userEndpoint.updatePasswordUser(session, "admin");
        Assert.assertNotEquals(oldPass, newPass);
        Assert.assertEquals(oldPass, HashUtil.salt("admin"));
        Assert.assertEquals(newPass, HashUtil.salt("newAdmin"));
    }

}
