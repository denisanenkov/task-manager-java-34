package ru.anenkov.tm.dto.result;

import ru.anenkov.tm.dto.result.Result;

public class Success extends Result {

    public Success() {
        success = true;
        message = "success";
        System.out.println("SUCCESS");
    }

}
