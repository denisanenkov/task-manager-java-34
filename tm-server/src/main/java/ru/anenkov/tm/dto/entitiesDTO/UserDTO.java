package ru.anenkov.tm.dto.entitiesDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.HashUtil;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "app_user")
public final class UserDTO extends AbstractEntityDTO {

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @Nullable
    private String email = "";

    @Nullable
    private String firstName = "";

    @Nullable
    private String middleName = "";

    @Nullable
    private String lastName = "";

    @NotNull
    public boolean isLocked() {
        return locked;
    }

    @NotNull
    public void setLocked(@NotNull boolean locked) {
        this.locked = locked;
    }

    @NotNull
    private boolean locked = false;

    @NotNull
    private Role role = Role.USER;

    public String getPasswordHash() {
        return HashUtil.salt(passwordHash);
    }

    public String getPassword() {
        return passwordHash;
    }

    @Override
    public String toString() {
        return "\nUser:\n" +
                "login ='" + login + '\'' +
                ", \npasswordHash ='" + passwordHash + '\'' +
                ", \nemail ='" + email + '\'' +
                ", \nfirstName ='" + firstName + '\'' +
                ", \nmiddleName ='" + middleName + '\'' +
                ", \nlastName ='" + lastName + '\'' +
                ", \nlocked =" + locked + "\n";
    }

    public UserDTO(@NotNull final User user) {
        if (user == null) return;
        login = user.getLogin();
        passwordHash = user.getPassword();
        firstName = user.getFirstName();
        middleName = user.getMiddleName();
        lastName = user.getLastName();
        locked = user.isLocked();
        role = user.getRole();
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    public UserDTO toDTO(@Nullable final User user) {
        if (user == null) return null;
        return new UserDTO(user);
    }

    @NotNull
    public static List<UserDTO> toDTO(@Nullable final Collection<User> users) {
        if (users == null || users.isEmpty()) return Collections.emptyList();
        List<UserDTO> result = new ArrayList<>();
        for (@Nullable final User user : users) {
            if (user == null) continue;
            result.add(new UserDTO(user));
        }
        return result;
    }

}
