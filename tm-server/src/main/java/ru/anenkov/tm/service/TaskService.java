package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.repository.TaskRepository;
import ru.anenkov.tm.api.service.ITaskService;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.exception.empty.EmptyUserIdException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.exception.empty.EmptyNameException;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;
import ru.anenkov.tm.exception.user.EntityConvertException;
import ru.anenkov.tm.api.repository.TaskRepository;
import ru.anenkov.tm.api.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService extends AbstractService<Task> implements ITaskService {

    @Nullable
    @Autowired
    private TaskRepository taskRepository;

    @Nullable
    @Autowired
    private UserService userService;

    @Override
    @Nullable
    @SneakyThrows
    public Task toTask(@Nullable final String userId, @Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) throw new EmptyEntityException();
        @Nullable final Task task = findOneByIdEntity(userId, taskDTO.getId());
        if (task == null) throw new EntityConvertException();
        return task;
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<Task> toTaskList(
            @Nullable final String userId,
            @Nullable final List<TaskDTO> taskDTOList
    ) {
        if (taskDTOList == null || taskDTOList.isEmpty()) throw new EmptyEntityException();
        @Nullable List<Task> result = new ArrayList<>();
        for (@Nullable final TaskDTO task : taskDTOList) {
            @Nullable Task currentTask = toTask(userId, task);
            result.add(currentTask);
        }
        if (result == null || result.isEmpty()) throw new EntityConvertException();
        return result;
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDTO toTaskDTO(@Nullable final Task task) {
        @Nullable TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        return taskDTO;
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<TaskDTO> toTaskDTOList(@Nullable final List<Task> taskList) {
        @Nullable List<TaskDTO> taskDTOS = new ArrayList<>();
        for (@Nullable final Task taskEntity : taskList) {
            @Nullable TaskDTO taskDTO = new TaskDTO();
            taskDTO.setUserId(taskEntity.getUser().getId());
            taskDTO.setName(taskEntity.getName());
            taskDTO.setDescription(taskEntity.getDescription());
            taskDTOS.add(taskDTO);
        }
        return taskDTOS;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> getListEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    @Nullable
    @Transactional(readOnly = true)
    public List<TaskDTO> getListDTOs(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return toTaskDTOList(taskRepository.findAllByUserId(userId));
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = new Task(name, userService.findByIdEntity(userId));
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = new Task(name, description, userService.findByIdEntity(userId));
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void remove(
            @Nullable String userId,
            @NotNull Task task
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyEntityException();
        taskRepository.deleteById(task.getId());
    }

    @Override
    @Transactional
    public void add(
            @Nullable String userId,
            @NotNull TaskDTO task
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyEntityException();
        task.setUserId(userId);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return toTaskDTOList(taskRepository.findAllByUserId(userId));
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteById(userId);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public TaskDTO findOneByIndexDTO(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        List<Task> tasks = taskRepository.findAllByUserId(userId);
        return toTaskDTO(tasks.get(index));
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public TaskDTO findOneByNameDTO(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return toTaskDTO(taskRepository.findByName(userId, name));
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public TaskDTO findOneByIdDTO(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return toTaskDTO(findOneByIdEntity(userId, id));
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Task findOneByIdEntity(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Task findOneByIndexEntity(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        List<Task> tasks = taskRepository.findAllByUserId(userId);
        return tasks.get(index);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Task findOneByNameEntity(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }


    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        List<Task> taskList = taskRepository.findAllByUserId(userId);
        taskRepository.delete(taskList.get(index));
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByName(
            final @Nullable String userId,
            final @Nullable String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.removeTasksByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.removeTasksByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.updateTaskById(userId, id, name, description);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateTaskByName(
            @Nullable final String userId,
            @Nullable final String oldName,
            @Nullable final String newName,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (oldName == null || oldName.isEmpty()) throw new IncorrectIndexException();
        if (newName == null || newName.isEmpty()) throw new EmptyNameException();
        taskRepository.updateTaskByName(userId, oldName, newName, description);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String newName,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (newName == null || newName.isEmpty()) throw new EmptyNameException();
        List<Task> tasks = taskRepository.findAllByUserId(userId);
        Task task = tasks.get(index);
        task.setName(newName);
        task.setDescription(description);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void load(@Nullable final List<Task> tasks) {
        taskRepository.saveAll(tasks);
    }

    @Override
    @Nullable
    @Transactional(readOnly = true)
    public List<Task> getList() {
        return getList();
    }

}