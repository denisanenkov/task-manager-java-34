package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.repository.UserRepository;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.user.EntityConvertException;
import ru.anenkov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService extends AbstractService<User> implements IUserService {

    @Nullable
    @Autowired
    private UserRepository userRepository;

    @Nullable
    @SneakyThrows
    public User toUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) throw new EmptyEntityException();
        @Nullable User user = findByIdEntity(userDTO.getId());
        if (user == null) throw new EntityConvertException("user");
        return user;
    }

    @Override
    @Nullable
    @SneakyThrows
    public User toUserFromOpt(@Nullable final Optional<User> user) {
        @Nullable User userEntity = new User();
        if (user == null) throw new EmptyEntityException();
        userEntity.setId(user.get().getId());
        userEntity.setLogin(user.get().getLogin());
        userEntity.setPasswordHash(user.get().getPasswordHash());
        userEntity.setFirstName(user.get().getFirstName());
        userEntity.setMiddleName(user.get().getMiddleName());
        userEntity.setLastName(user.get().getLastName());
        userEntity.setLocked(user.get().isLocked());
        userEntity.setEmail(user.get().getEmail());
        userEntity.setRole(user.get().getRole());
        return userEntity;
    }

    @Override
    @Nullable
    @SneakyThrows
    public UserDTO toUserDTO(@Nullable final User user) {
        @Nullable UserDTO userDTO = new UserDTO();
        if (user == null) throw new EmptyEntityException();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setMiddleName(user.getMiddleName());
        userDTO.setLastName(user.getLastName());
        userDTO.setLocked(user.isLocked());
        userDTO.setEmail(user.getEmail());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<UserDTO> toUserDTOList(@Nullable final List<User> userList) {
        if (userList == null || userList.isEmpty()) throw new EmptyEntityException();
        @Nullable List<UserDTO> userDTOS = new ArrayList<>();
        for (@Nullable User user : userList) {
            @Nullable UserDTO userDTO = new UserDTO();
            userDTO.setId(user.getId());
            userDTO.setLogin(user.getLogin());
            userDTO.setPasswordHash(user.getPasswordHash());
            userDTO.setFirstName(user.getFirstName());
            userDTO.setMiddleName(user.getMiddleName());
            userDTO.setLastName(user.getLastName());
            userDTO.setLocked(user.isLocked());
            userDTO.setEmail(user.getEmail());
            userDTO.setRole(user.getRole());
            userDTOS.add(userDTO);
        }
        return userDTOS;
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<User> findAllEntities() {
        return userRepository.findAll();
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<UserDTO> findAllDTO() {
        return toUserDTOList(userRepository.findAll());
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public UserDTO findByIdDTO(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return toUserDTO(findByIdEntity(id));
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findByIdEntity(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return toUserFromOpt(userRepository.findById(id));
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public UserDTO findByLoginDTO(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return toUserDTO(userRepository.findByLogin(login));
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findByLoginEntity(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findByEmailEntity(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeUser(@Nullable final User user) {
        if (user == null) throw new EmptyUserException();
        userRepository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyUserException();
        userRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.deleteByEmail(email);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable User user = new User(login, HashUtil.salt(password));
        userRepository.save(user);
    }


    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable User user = new User(login, HashUtil.salt(password), email);
        userRepository.save(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable User user = new User(login, HashUtil.salt(password), role);
        userRepository.save(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void updateUserFirstName(
            @Nullable final String userId,
            @Nullable final String newFirstName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyPasswordException();
        userRepository.updateFirstNameUserByUserId(newFirstName, userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void updateUserMiddleName(
            @Nullable final String userId,
            @Nullable final String newMiddleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyParameterException();
        userRepository.updateMiddleNameUserByUserId(newMiddleName, userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void updateUserLastName(
            @Nullable final String userId,
            @Nullable final String newLastName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyParameterException();
        userRepository.updateLastNameUserByUserId(newLastName, userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void updateUserEmail(
            @Nullable final String userId,
            @Nullable final String newEmail
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyParameterException();
        userRepository.updateEmailUserByUserId(newEmail, userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String newPassword
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        userRepository.updatePasswordUserByUserId(HashUtil.salt(newPassword), userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.setLockedUser(login);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.setUnlockedUser(login);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void deleteUserByLogin(@Nullable final String login) {
        userRepository.deleteByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<User> getList() {
        return findAllEntities();
    }

}
