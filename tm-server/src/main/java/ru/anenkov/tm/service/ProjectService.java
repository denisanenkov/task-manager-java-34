package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.repository.ProjectRepository;
import ru.anenkov.tm.api.service.IProjectService;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.exception.system.IncorrectDataException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.exception.user.EntityConvertException;
import ru.anenkov.tm.api.repository.ProjectRepository;
import ru.anenkov.tm.api.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @Nullable
    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    @Autowired
    private UserService userService;

    @Override
    @SneakyThrows
    public Project toProject(@Nullable final String userId, @Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) throw new EmptyEntityException();
        @Nullable final Project project = findOneByIdEntity(userId, projectDTO.getId());
        if (project == null) throw new EntityConvertException();
        return project;
    }

    @Override
    @SneakyThrows
    public ProjectDTO toProjectDTO(@Nullable final Project project) {
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        return projectDTO;
    }

    @Override
    @SneakyThrows
    public Project toProjectFromOpt(@Nullable final Optional<Project> projectOptional) {
        @Nullable Project project = new Project();
        project.setUser(projectOptional.get().getUser());
        project.setName(projectOptional.get().getName());
        project.setDescription(projectOptional.get().getDescription());
        return project;
    }

    @Override
    @SneakyThrows
    public List<ProjectDTO> toProjectDTOList(@Nullable final List<Project> projectList) {
        if (projectList == null || projectList.isEmpty()) throw new EmptyListException();
        @Nullable List<ProjectDTO> projectDTOS = new ArrayList<>();
        for (Project projectEntity : projectList) {
            @Nullable ProjectDTO projectDTO = new ProjectDTO();
            projectDTO.setUserId(projectEntity.getUser().getId());
            projectDTO.setName(projectEntity.getName());
            projectDTO.setDescription(projectEntity.getDescription());
            projectDTOS.add(projectDTO);
        }
        return projectDTOS;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUser(userService.findByIdEntity(userId));
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(userService.findByIdEntity(userId));
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(
            @Nullable final String userId,
            @Nullable final Project project
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        project.setUser(userService.findByIdEntity(userId));
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(
            @Nullable final String userId,
            @Nullable final Project project
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyEntityException();
        if (project.getUser().getId() != userId) throw new IncorrectDataException();
        projectRepository.delete(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Project> findAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.deleteById(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public ProjectDTO findOneByIndexDTO(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return toProjectDTO(findOneByIndexEntity(userId, index));
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public @Nullable ProjectDTO findOneByIdDTO(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return toProjectDTO(findOneByIdEntity(userId, id));
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public ProjectDTO findOneByNameDTO(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return toProjectDTO(projectRepository.findByName(userId, name));
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public Project findOneByIdEntity(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional(readOnly = true)
    public Project findOneByIndexEntity(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        List<Project> projectList = projectRepository.findAllByUserId(userId);
        return projectList.get(index);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public @Nullable Project findOneByNameEntity(@Nullable String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        List<Project> projectList = projectRepository.findAllByUserId(userId);
        projectRepository.delete(projectList.get(index));
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        projectRepository.removeProjectsByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.removeProjectsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<ProjectDTO> findAllDTOs(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return toProjectDTOList(projectRepository.findAllByUserId(userId));
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(
            @Nullable final String userId,
            @Nullable final ProjectDTO projectDTO
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectDTO == null) throw new EmptyIdException();
        projectRepository.delete(new ProjectService().toProject(userId, projectDTO));
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        List<Project> projects = projectRepository.findAllByUserId(userId);
        Project project = projects.get(index);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        projectRepository.updateProjectById(userId, id, name, description);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateProjectByName(
            @Nullable String userId,
            @NotNull String oldName,
            @NotNull String newName,
            @NotNull String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (oldName == null || oldName.isEmpty()) throw new IncorrectIndexException();
        if (newName == null || newName.isEmpty()) throw new EmptyNameException();
        projectRepository.updateProjectByName(userId, oldName, newName, description);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Project> getList() {
        return (List<Project>) projectRepository.findAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void load(@Nullable final List<Project> projects) {
        projectRepository.saveAll(projects);
    }

}
