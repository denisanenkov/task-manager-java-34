package ru.anenkov.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.endpoint.*;

import javax.xml.ws.Endpoint;

@Component
@NoArgsConstructor
public final class Bootstrap {

    @Autowired
    private AbstractEndpoint[] endpointList;

    private void initEndpoint() {
        for (@Nullable final AbstractEndpoint endpoint : endpointList) {
            registryEndpoint(endpoint);
        }
    }

    private void registryEndpoint(final Object endpoint) {
        if (endpoint == null) return;
        final String host = DataConst.SERVER_HOST;
        final String port = DataConst.SERVER_PORT;
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        Endpoint.publish(wsdl, endpoint);
        System.out.println(wsdl);
    }

    public void run(@Nullable final String[] args) {
        initEndpoint();
        System.out.println("\t\t***SERVERS*STARTED***");
        if (args == null || args.length != 0) System.exit(0);
    }

}