package ru.anenkov.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.config.ApplicationConfiguration;

public class Application {

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @Nullable final ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
