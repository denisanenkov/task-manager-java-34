package ru.anenkov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.HashUtil;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties
@Entity
@Table(name = "app_user")
public class User extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    @Column(unique = true, nullable = false)
    private String login = "";

    @Nullable
    private String passwordHash = "";

    @Nullable
    private String email = "";

    @Nullable
    private String firstName = "";

    @Nullable
    private String middleName = "";

    @Nullable
    private String lastName = "";

    @NotNull
    private boolean locked = false;

    @NotNull
    public boolean isLocked() {
        return locked;
    }

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

    @NotNull
    public void setLocked(@NotNull boolean locked) {
        this.locked = locked;
    }

    public String getPasswordHash() {
        return HashUtil.salt(passwordHash);
    }

    public String getPassword() {
        return passwordHash;
    }

    public User(String login, String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = Role.USER;
    }

    public User(@Nullable String login, @Nullable String passwordHash, Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    public User(@Nullable String login, @Nullable String passwordHash, String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Session> sessions = new ArrayList<>();

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", locked=" + locked +
                ", role=" + role +
                '}';
    }

}
