package ru.anenkov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
@Entity
@Table(name = "app_task")
public class Task extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @ManyToOne
    private Project project;

    public Task(
            final @Nullable String name,
            final @Nullable String description,
            final @Nullable User user,
            final @Nullable Project project) {
        this.name = name;
        this.description = description;
        this.user = user;
        this.project = project;
    }

    public Task(
            final @Nullable String name,
            final @Nullable User user,
            final @Nullable Project project
    ) {
        this.name = name;
        this.user = user;
        this.project = project;
    }

    public Task(
            final @Nullable String name,
            final @Nullable String description
    ) {
        this.name = name;
        this.description = description;
    }

    public Task(
            final @Nullable String name,
            final @Nullable User user
    ) {
        this.name = name;
        this.user = user;
    }

    public Task(
            final @Nullable String name,
            final @Nullable String description,
            final @Nullable User user
    ) {
        this.name = name;
        this.description = description;
        this.user = user;
    }

    public static Task toTask(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return null;
        return new Task(taskDTO.getName(), taskDTO.getDescription());
    }

    @Override
    public String toString() {
        return "\nTask:\n" +
                "\nname = '" + name + '\'' +
                ", \ndescription ='" + description + '\'' +
                ", \nuserId ='" + user.getId() + '\'' +
                "\n";
    }

}
