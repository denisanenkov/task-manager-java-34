package ru.anenkov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("Пользователь"),
    ADMIN("Администратор");

    @NotNull
    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
