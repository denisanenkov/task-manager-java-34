package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.anenkov.tm.api.endpoint.IAdminUserEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.service.SessionService;
import ru.anenkov.tm.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class AdminUserEndpoint extends AdminEndpoint implements IAdminUserEndpoint {

    @Nullable
    @Autowired
    private SessionService sessionService;

    @Nullable
    @Autowired
    private UserService userService;

    @WebMethod
    @Override
    @SneakyThrows
    public void lockUserByLogin(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        userService.lockUserByLogin(login);
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void unlockUserByLogin(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        userService.unlockUserByLogin(login);
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void deleteUserByLogin(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        userService.deleteUserByLogin(login);
    }

    @WebMethod
    @Override
    @NotNull
    @SneakyThrows
    public void removeUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "user") UserDTO user
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        userService.removeUser
                (userService.toUser(user));
    }

    @WebMethod
    @Override
    @NotNull
    @SneakyThrows
    public void removeByIdUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        userService.removeById(id);
    }

    @WebMethod
    @Override
    @NotNull
    @SneakyThrows
    public void removeByLoginUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        userService.removeByLogin(login);
    }

    @WebMethod
    @Override
    @NotNull
    @SneakyThrows
    public void removeByEmailUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "email") String email
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        userService.removeByEmail(email);
    }

}
