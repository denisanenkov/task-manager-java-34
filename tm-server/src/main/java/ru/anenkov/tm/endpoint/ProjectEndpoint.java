package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.anenkov.tm.api.endpoint.IProjectEndpoint;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.service.SessionService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Nullable
    @Autowired
    private SessionService sessionService;

    @Nullable
    @Autowired
    private ProjectService projectService;

    @WebMethod
    @SneakyThrows
    @Override
    public void createProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void addProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "tasks") final Project project
    ) {
        sessionService.validate(session);
        projectService.add(session.getUserId(), project);
    }

    @WebMethod
    @SneakyThrows
    public void removeProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "tasks") final ProjectDTO project
    ) {
        sessionService.validate(session);
        projectService.remove(session.getUserId(), project);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public List<ProjectDTO> findAllProjects(
            @NotNull @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        return projectService.toProjectDTOList(projectService.findAllEntities(session.getUserId()));
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void clearProjects(
            @NotNull @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public ProjectDTO findOneByIndexProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return projectService.toProjectDTO
                (projectService.findOneByIndexEntity(session.getUserId(), index));
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public ProjectDTO findOneByNameProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return projectService.toProjectDTO
                (projectService.findOneByNameEntity(session.getUserId(), name));
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public ProjectDTO findOneByIdProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return projectService.toProjectDTO
                (projectService.findOneByIdEntity(session.getUserId(), id));
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public void removeOneByIndexProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        projectService.removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void removeOneByNameProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        projectService.removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void removeOneByIdProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        projectService.removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void updateByIdProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id") final String id,
            @NotNull @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        projectService.updateProjectById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void updateByIndexProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index") final Integer index,
            @NotNull @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        projectService.updateProjectByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void loadProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "tasks") final List project
    ) {
        sessionService.validate(session);
        projectService.load(project);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public List<ProjectDTO> getListProjects(
            @NotNull @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        List<Project> projects = projectService.findAllEntities(session.getUserId());
        return projectService.toProjectDTOList(projects);
    }

}
