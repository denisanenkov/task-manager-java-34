package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.anenkov.tm.api.endpoint.ICalcEndpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService(endpointInterface = "ru.anenkov.tm.api.endpoint.ICalcEndpoint")
@Controller
public class CalcEndpoint extends AbstractEndpoint implements ICalcEndpoint {

    public static final String URL = "http://localhost:8080/Calculator?wsdl";

    @WebMethod
    @SneakyThrows
    public int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    ) {
        return a + b;
    }

}
