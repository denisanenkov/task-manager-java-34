package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.user.EntityConvertException;

import java.util.List;

public interface ISessionService extends IService<Session> {

    Session toSession(SessionDTO sessionDTO);

    SessionDTO toSessionDTO(Session session);

    List<SessionDTO> toSessionDTOList(List<Session> session);

    void close(Session session);

    void closeAll(Session session);

    void close(SessionDTO session);

    void closeAll(SessionDTO session);

    void closeAll(@Nullable final String userId);

    Session findSessionById(String id);

    UserDTO getUser(SessionDTO session);

    String getUserId(SessionDTO session);

    String sign(Session session);

    boolean isValid(SessionDTO session);

    void validate(SessionDTO session);

    void validate(SessionDTO session, Role role);

    void validate(Session session);

    void validate(Session session, Role role);

    Session open(String login, String password);

    boolean checkUserAccess(String login, String password);

    void clearAll(@Nullable final String userId);

}
