package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

import java.util.List;
import java.util.Optional;

public interface IUserService extends IService<User> {

    @Nullable
    List<UserDTO> toUserDTOList(@Nullable List<User> userList);

    @Nullable
    UserDTO toUserDTO(@Nullable User user);

    User toUserFromOpt(@Nullable final Optional<User> user);

    @Nullable
    List<User> findAllEntities();

    @Nullable
    List<UserDTO> findAllDTO();

    @Nullable
    void create(String login, String password);

    void create(String login, String password, String email);

    void create(String login, String password, Role role);

    @Nullable
    UserDTO findByIdDTO(String id);

    @Nullable
    User findByIdEntity(String id);

    @Nullable
    UserDTO findByLoginDTO(String login);

    @Nullable
    User findByLoginEntity(String login);

    @Nullable
    void updateUserFirstName(@Nullable String userId, @Nullable String newFirstName);

    @Nullable
    void updateUserMiddleName(@Nullable String userId, @Nullable String newMiddleName);

    @Nullable
    void updateUserLastName(@Nullable String userId, @Nullable String newLastName);

    @Nullable
    void updateUserEmail(@Nullable String userId, @Nullable String newEmail);

    @Nullable
    void updatePassword(@Nullable String userId, @Nullable String newPassword);

    @Nullable List<User> getList();

    void removeUser(User user);

    void removeById(String id);

    void removeByLogin(String login);

    void removeByEmail(String email);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

    void deleteUserByLogin(@NotNull String login);

    void load(@Nullable User... users);

}
