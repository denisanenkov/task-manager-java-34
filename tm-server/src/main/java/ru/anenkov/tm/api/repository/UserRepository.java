package ru.anenkov.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.repository.UserRepository;
import ru.anenkov.tm.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, String>, JpaRepository<User, String> {

    @Transactional(readOnly = true)
    User findByLogin(@NotNull final String login);

    @Transactional(readOnly = true)
    User findByEmail(@NotNull final String email);

    @Transactional
    void deleteById(@NotNull final String id);

    @Transactional
    void deleteByLogin(@NotNull final String login);

    @Transactional
    void deleteByEmail(@NotNull final String email);

    @Modifying
    @SneakyThrows
    @Transactional
    @Query(value = "UPDATE app_user SET firstname = :firstName WHERE id = :userId", nativeQuery = true)
    int updateFirstNameUserByUserId(
            @Param("firstName") @NotNull final String newFirstName,
            @Param("userId") @NotNull final String userId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE app_user SET middlename = :middleName WHERE id = :userId", nativeQuery = true)
    void updateMiddleNameUserByUserId(
            @Param("middleName") @NotNull final String newMiddleName,
            @Param("userId") @NotNull final String userId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE app_user SET lastname = :lastName WHERE id = :userId", nativeQuery = true)
    void updateLastNameUserByUserId(
            @Param("lastName") @NotNull final String newLastName,
            @Param("userId") @NotNull final String userId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE app_user SET email = :email WHERE id = :userId", nativeQuery = true)
    void updateEmailUserByUserId(
            @Param("email") @NotNull final String newEmail,
            @Param("userId") @NotNull final String userId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE app_user SET passwordhash = :password WHERE id = :userId", nativeQuery = true)
    void updatePasswordUserByUserId(
            @Param("password") @NotNull final String newPassword,
            @Param("userId") @NotNull final String userId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE app_user SET locked = '1' WHERE login = :login", nativeQuery = true)
    void setLockedUser(
            @Param("login") @NotNull final String login);

    @Modifying
    @Transactional
    @Query(value = "UPDATE app_user SET locked = '0' WHERE login = :login", nativeQuery = true)
    void setUnlockedUser(
            @Param("login") @NotNull final String login);

}
