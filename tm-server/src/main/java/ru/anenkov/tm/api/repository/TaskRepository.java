package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.repository.TaskRepository;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.entity.Task;

import java.util.*;

@Repository
public interface TaskRepository extends CrudRepository<Task, String> {

    @Transactional(readOnly = true)
    List<Task> findAllByUserId(@NotNull final String userId);

    @Transactional
    void removeTasksByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Transactional
    void removeTasksByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    @Transactional(readOnly = true)
    @Query(value = "SELECT * FROM app_task WHERE user_id = :userId AND name = :name", nativeQuery = true)
    Task findByName(
            @Param("userId") @NotNull final String userId,
            @Param("name") @NotNull final String name);

    @Transactional(readOnly = true)
    @Query(value = "SELECT * FROM app_task WHERE user_id = :userId AND id = :id", nativeQuery = true)
    Task findById(
            @Param("userId") @NotNull final String userId,
            @Param("id") @NotNull final String id);

    @Modifying
    @Transactional
    @Query(value = "UPDATE app_task SET name = :name, description = :description WHERE user_id = :userId AND id = :id", nativeQuery = true)
    void updateTaskById(
            @Param("userId") @NotNull final String userId,
            @Param("id") @NotNull final String id,
            @Param("name") @NotNull final String name,
            @Param("description") @NotNull final String description);

    @Modifying
    @Transactional
    @Query(value = "UPDATE app_task SET name = :name, description = :description WHERE user_id = :userId AND name = :oldName", nativeQuery = true)
    void updateTaskByName(
            @Param("userId") @NotNull final String userId,
            @Param("oldName") @NotNull final String oldName,
            @Param("name") @NotNull final String name,
            @Param("description") @NotNull final String description);

}
