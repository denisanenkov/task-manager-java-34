package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.repository.ProjectRepository;
import ru.anenkov.tm.entity.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends CrudRepository<Project, String> {

    @Transactional(readOnly = true)
    List<Project> findAllByUserId(@NotNull final String userId);

    @Transactional
    void removeProjectsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Transactional
    void removeProjectsByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    @Transactional(readOnly = true)
    @Query(value = "SELECT * FROM app_project WHERE user_id = :userId AND name = :name", nativeQuery = true)
    Project findByName(
            @Param("userId") @NotNull final String userId,
            @Param("name") @NotNull final String name);

    @Transactional(readOnly = true)
    @Query(value = "SELECT * FROM app_project WHERE user_id = :userId AND id = :id", nativeQuery = true)
    Project findById(
            @Param("userId") @NotNull final String userId,
            @Param("id") @NotNull final String id);

    @Modifying
    @Transactional
    @Query(value = "UPDATE app_project SET name = :name, description = :description WHERE user_id = :userId AND id = :id", nativeQuery = true)
    void updateProjectById(
            @Param("userId") @NotNull final String userId,
            @Param("id") @NotNull final String id,
            @Param("name") @NotNull final String name,
            @Param("description") @NotNull final String description);

    @Modifying
    @Transactional
    @Query(value = "UPDATE app_project SET name = :name, description = :description WHERE user_id = :userId AND name = :oldName", nativeQuery = true)
    void updateProjectByName(
            @Param("userId") @NotNull final String userId,
            @Param("oldName") @NotNull final String oldName,
            @Param("name") @NotNull final String name,
            @Param("description") @NotNull final String description);

}
