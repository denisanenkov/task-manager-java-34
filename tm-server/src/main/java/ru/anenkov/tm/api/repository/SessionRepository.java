package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.repository.SessionRepository;
import ru.anenkov.tm.entity.Session;

import java.util.List;

@Repository
public interface SessionRepository extends CrudRepository<Session, String> {

    @Transactional
    void deleteAllById(@NotNull final String id);

    @Transactional (readOnly = true)
    void findAllById(@NotNull final String id);

    @Transactional (readOnly = true)
    @Query(value = "SELECT * FROM app_session", nativeQuery = true)
    List<Session> findAll();

    @Transactional (readOnly = true)
    @Query(value = "SELECT * FROM app_session WHERE id = :id", nativeQuery = true)
    Session findSessionById(@Param("id") @NotNull final String id);

}
