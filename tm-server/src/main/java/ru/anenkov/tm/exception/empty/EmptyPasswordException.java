package ru.anenkov.tm.exception.empty;

import ru.anenkov.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty!");
    }

}
