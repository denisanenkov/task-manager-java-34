package ru.anenkov.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.exception.AbstractException;

public class EntityConvertException extends AbstractException {

    public EntityConvertException() {
        System.out.println("Error! Exception of convert entity..");
    }

    public EntityConvertException(@NotNull String message) {
        super("Error! Convert entity/dto in " + message + " exception..");
    }

}
